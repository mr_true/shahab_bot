<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;



class MainController extends Controller
{

public function index(Request $request)
{

$sign_up_strings    = array(
"qu1" =>  "طرح خوشه‌بندی شهاب با هدف ایجاد نشاط علمی و بسترسازی فرایند پیشرفت دانشجویان مستعد دانشگاه فردوسی مشهد، توسط شورای هدایت استعدادهای برتر (شهاب) طراحی و به اجرا درآمده است. جامعه هدف در این طرح، ۱۰ درصد برتر دانشجویان همه مقاطع تحصیلی است که با نگاه جامع‎نگر به حوزه‎ استعدادها، در برگیرنده نتایج برجسته در فعالیت‎های آموزشی، پژوهشی، فناوری و اجتماعی این دانشجویان است.
در این طرح، ۱۰ درصد برتر دانشجویان دانشگاه فردوسی مشهد در سه خوشه به تفکیک شش درصد در خوشه سه، سه درصد در خوشه دو و تنها یک درصد در خوشه یک قرار می‌گیرند. با توجه به تجمیع کلیه تسهیلات اعطایی مربوط به دانشجویان مستعد دانشگاه در قالب این طرح برای دانشجویان برگزیده تسهیلات ویژه‌ای در حوزه رفاهی شامل خوابگاه، امکانات ورزشی، تغذیه و در حوزه علمی شامل دستیاری آموزشی، پژوهشی و فناوری، حمایت از شرکت در همایش‌ها و کارگاه‌های ملی و بین المللی، حمایت از دوره‌های فرصت مطالعاتی، مسابقات علمی معتبر، المپیادهای دانشجویی و برنامه‌های توانمندسازی، در نظر گرفته شده است.",
"qu2" =>  "دانشجویان در صورت دارا بودن یکی از شروط پایین، می‌توانند در این طرح ثبت‌‌نام  نمایند:

۱- رتبه برتر در آزمون‌های سراسری (کارشناسی: ۵/۲ انحراف معیار اعلامی توسط سازمان سنجش؛ کارشناسی‌ارشد: رتبه‌های اول تا بیستم در هر کد رشته؛ آزمون جامع پیش درمانگاهی دکتری حرفه‌ای: رتبه‌های اول تا سوم)، المپیاد های علمی دانش‌آموزی و دانشجویی ملی و بین المللی (رتبه اول تا پانزدهم)، جشنواره‌ها و مسابقات علمی معتبر ملی یا بین المللی
۲- دانشجوی نمونه كشوری
۳- ثبت اختراع ملی و بین المللی
۴- برگزیده قرآنی، ادبی، هنری و ورزشی در سطح ملی یا بین المللی
۵- كسب معدل برتر (۱۰ درصد برتر هر کدرشته-گرایش) در مقاطع جاری تحصیل (كارشناسی، كارشناسی ارشد)
۶- پذیرش بدون آزمون در مقاطع كارشناسی ارشد یا دكتری (سهمیه استعدادهای درخشان)
۷- حائز برتری نسبی در میان دانشجویان هر رشته-گرایش در بروندادهای پژوهشی شامل انتشار مقالات در نشریات معتبر (ISI) علمی-پژوهشی و علمی-ترویجی مورد تایید  وزارت علوم
۸- فعالیت‌های تأثیر‌گذار اجتماعی (عضویت فعال و مؤثر در انجمن‌ها و نهادهای اجتماعی با تایید  بالاترین نهاد مسئول در نهاد مربوطه)
۹- بند ویژه

تذکر مهم: با توجه به بند ۵ فراخوان تنها کسب معدل برتر (۱۰ درصد برتر در رشته-گرایش) در مقطع فعلی تحصیل دانشجو و در مقاطع کارشناسی و کارشناسی‌ارشد ملاک است. این بند برای دانشجویان دکتری تخصصی نیست.
دانشجویان دکتری حرفه‎ای نیز قبل از آزمون جامع، معادل با کارشناسی و بعد از آن، معادل با دانشجویان ارشد در نظر گرفته خواهند شد.
بند ویژه: دانشجو شرایط هیچ یک از بند های ۱ الی ۸ را دارا نیست اما با توجه به شرایط ویژه و برجسته‌ای که به صورت خلاصه عنوان می‌شود، خواستار ثبت‌‌نام  در طرح است.
تذکرات مهم مربوط به بند ویژه:
تذکر ۱ :انتخاب بند ویژه توسط دانشجو به معنای نداشتن شرایط بندهای ۱ الی ۸ است. در این حالت پرونده دانشجو تنها با توجه به شرایط بند ویژه بررسی و داوری خواهد شد.
تذکر ۲: در صورتی که دارای افتخارت ویژه‌ای هستید و شرایط یکی از بند های ۱ الی ۸ را نیز دارید نیاز به انتخاب بند ویژه نیست و می‎توانید مدارک مربوط به افتخارات ویژه را در حین فرایند ثبت‌‌نام  در قسمت‌های در نظر گرفته شده، وارد نمایید.
تذکر ۳: دانشجویانی که بند ویژه را انتخاب می‌کنند، مدارک خود را در قسمت افتخارات، گزینه « سایر افتخارات » بارگذاری نمایند.
تذکر ۴: پرونده دانشجویان متقاضی بند ویژه، مانند سایر دانشجویان به صورت رقابتی بررسی شده و مورد داوری قرار خواهد گرفت.",

"qu3" =>  "بله. بایستی بندهای مذكور را علامت زده و در هنگام ورود به فرایند ثبت‌‌نام  نسبت به پیوست گواهی و مدارك معتبر برای هر یك از بندها اقدام نمایید.
توجه داشته باشید كه به دلیل رقابتی بودن فرایند انتخاب، پس از احراز شرایط بندهای مذكور، كل مدارك و پرونده دانشجویان در طی دوران تحصیلی به صورت تجمیعی مورد بررسی و ارزیابی قرار خواهد گرفت.",
"qu4" =>  "این بند (بند ۷) مربوط به مقالات منتشر شده در نشریات معتبر (علمی-پژوهشی و یا علمی-ترویجی) مورد تایید  وزارت علوم است. داشتن مقالات در ژورنال‌های معتبر، موجب احراز برتری نسبی فرد در میان دانشجویان رشته-گرایش می‌گردد و می‌تواند امتیازی برای دانشجو، جهت ورود به طرح خوشه‌بندی شهاب باشد. بعد از تایید  بند مورد نظر جهت ورود به طرح خوشه‌بندی، در فرایند ثبت‌‌نام  باید مقالات خود را در قسمت مورد نظر آپلود نمایید.",
"qu5" =>  "سنوات مجاز برای شرکت در طرح خوشه‌بندی در مقاطع کارشناسی، کارشناسی‌ارشد، دکتری حرفه‌ای و دکتری تخصصی به ترتیب: چهارسال، دوسال، شش سال و چهار و نیم سال است.",
"qu6" =>  "اگر دانشجو فارغ‌التحصیل گردد و بلافاصله در مقاطع بالاتر در دانشگاه فردوسی پذیرفته شود مشمول دریافت بسته‌های تشویقی خواهد بود. اما چنانچه دانشجو فارغ التحصیل گردد و در مقاطع بالاتر پذیرفته نشود یا در دانشگاه دیگری پذیرفته شود، در صورت قرارگرفتن در خوشه‌های یک یا دو برای دانشجو گواهی صادر می‌شود که می‌تواند به عنوان رزومه علمی از آن استفاده نماید. همچنین اگر دانشجو فارغ التحصیل نگردد می‌تواند از بسته‌های تشویقی شهاب استفاده نماید.",
"qu7" =>  "بله. دانشجویان محترم می‌بایست مطابق با دستورالعمل‌های ارایه شده توسط بنیاد ملی نخبگان و در بازه زمانی که هرسال توسط بنیاد ملی نخبگان اعلام می‌گردد، نسبت به بارگذاری مدارك و ثبت درخواست خود اقدام نمایند.
تذكر مهم: با توجه به اینكه دانشجویان برگزیده جایزه تحصیلی بنیاد ملی نخبگان، تنها از طریق بنیاد ملی نخبگان مشخص خواهند شد، لذا عدم ثبت‌‌نام  به موقع و بارگذاری مدارك در سامانه سینا (یا به روز رسانی مدارك برای دانشجویانی كه قبلاً در سامانه سینا ثبت‌‌نام  كرده‌اند)، به منزله عدم درخواست تلقی شده و پذیرفته‎شدن در طرح خوشه‌بندی هیچ­گونه حقی برای دانشجو جهت بهره‌مندی از جوایز تحصیلی بنیاد نخبگان ایجاد نخواهد كرد.",
"qu8" =>  "دانشجویان پس از اعلام نتایج و مشخص شدن وضعیت خود می‌توانند از شهریور سالی که ثبت‌‌نام  طرح صورت می‌گیرد تا شهریور سال بعد جهت بهره‌مندی از این بسته‌های تشویقی اقدام نمایند.",
"qu9" =>  " رتبه معدل عبارت است از رتبه شما در میان دانشجویان هم ورودی از نظر معدل كل كه به عنوان مثال اول، ‏دوم و ... باشد. ۱۰ درصد برتر نیز با توجه به تعداد دانشجویان هم ورودی (کد رشته ورودی)  تعیین می‌شود.",

"qu10" => "بله. دانشجو می تواند با مراجعه به دفتر استعداد درخشان دانشگاه فردوسی، گواهی پذیرش بدون آزمون را دریافت نماید.",

"qu11" => "رتبه معدل عبارت است از رتبه شما در میان دانشجویان هم ورودی از نظر معدل كل كه به عنوان مثال اول، ‏دوم و ... باشد. ۱۰ درصد برتر نیز با توجه به تعداد دانشجویان هم ورودی (کد رشته ورودی) تعیین می‌شود. ",
"qu12" => " گواهی پذیرش بدون آزمون در مقاطع كارشناسی ارشد یا دكتری را از دفتر هدایت استعداهای درخشان دانشگاه درخواست ‏نمایید.",

"qu13" => "بله. دانشجویان در هر دوره از طرح، می‌بایست نسبت به ثبت‌نام از طریق پرتال آموزشی خود اقدام نمایند. برای آن دسته از دانشجویان که در دوره‌های قبلی طرح شرکت نموده‌اند نیازی به بارگذاری مدارک قبلی نیست و سابقه آن در پرونده موجود است. تنها در صورتی که در طی سال جاری مدارک و افتخارات جدیدی دارند بایستی بارگذاری نمایند تا مورد بررسی قرار گیرد. حتی دانشجویانی که مدارک جدیدی ندارند نیز باید دوباره در طرح ثبت‌‌نام  نمایند.
این دانشجویان با مراجعه به پرتال دانشجویی، قسمت فعالیتهای آموزشی، ثبت‌‌نام  طرح خوشه‌بندی شهاب و انتخاب گزینه \"طرح خوشه‌بندی شهاب\" -دوره جدید- به پرونده خود دسترسی داشته و می‌توانند مدارک جدید را بارگذاری کنند.
تایید  نهایی پرونده الزامی است و تنها در این صورت پرونده در طرح ثبت خواهد شد.",

"qu14" => "بله، هر یک از دانشجویان دانشگاه فردوسی که دارای شماره دانشجویی می‌باشند، می‌توانند در این طرح شرکت نمایند",
"qu15" => "
خیر. تنها افتخارات کسب شده در دوران دانشجویی ملاک ارزیابی خواهد بود.",
"qu16" => "بله. بنیاد ملی نخبگان اقدام به بروزرسانی سامانه خود نموده است و کلیه فعالیت‌های نخبگانی در سامانه جدید سینا انجام می‌گیرد. چنانچه در سامانه قبلی (ثریا) مدارک خود را بارگذاری نموده‌اید اطلاعات شما در سامانه سینا موجود است.
کاربرانی که در سامانه ثریا عضو بودند ابتدا به منوی اشخاص - مشخصات متقاضی مراجعه و پس از تکمیل اطلاعات و انتخاب حوزه فعالیت  خود می‌توانند به منوهای آن دسترسی پیدا کنند و نسبت به تکمیل و ویرایش مدارک خود اقدام نمایند.",
"qu17" => "بله، برگزیدگان فعالیت‌های قرآنی، هنری، ورزشی و ادبی در مسابقات معتبر ملی و بین المللی می‎توانند در این طرح شرکت کنند.",
"qu18" => "بعد از ثبت‌‌نام  نهایی، پرونده دانشجویان وارد داوری در سه سطح گروه، دانشکده و دانشگاه می‌شود و پس از آن نتایج نهایی در اواسط آبان ماه اعلام می گردد.",
"qu19" => "چنانچه به نتیجه خود اعتراض دارید بایستی پس از مشاهده نتایج، در بازه زمانی اعلام شده، از طریق پرتال دانشجویی خود درخواست تجدید نظر کنید.",
"qu20" => "رزومه دانشجویان در تمامی زمینه‌ها شامل پژوهشی، آموزشی، فرهنگی و ... مورد ارزیابی قرار می‌گیرد و بر اساس امتیازات به‌دست آمده در یکی از خوشه‌ها قرار می‌گیرند.",
"qu21" => "‫
دانشکده کشاورزی: جناب آقای دکتر علی جوادمنش
دانشکده ادبیات: جناب آقای دکتر حمداله سجاسی قیداری
دانشکده الهیئت: جناب آقای دکتر وحید خادم زاده
دانشکده دامپزشکی: جناب آقای دکتر محمد عزیززاده
دانشکده علوم: سرکار خانم دکتر مشکینی
دانشکده علوم اداری و اقتصاد: جناب آقای دکتر مهدی فیضی
دانشکده علوم تربیتی: جناب آقای دکتر حسن بهزادی
دانشکده علوم ریاضی: جناب آقای دکتر پرویزی
دانشکده علوم ورزشی: سرکار خانم دکتر فاطمه علیرضائی نقندر
دانشکده معماری: جناب آقای دکتر هومن قهرمانی
دانشکده منابع طبیعی: سرکار خانم دکتر آزیتا فراشی
دانشکده مهندسی: سرکار خانم دکتر زهرا مقصود
",
"qu22" => "
- پاسخگویی از طریق ایمیل
دانشجویان محترم توجه داشته باشند که در صورت داشتن هرگونه سوال  می‌توانند از طریق ایمیل و با ذکر عنوان \"طرح خوشه‌بندی شهاب\"  اقدام نمایند. Talent2@um.ac.ir
- پاسخگویی تلفنی
در صورت دریافت نکردن پاسخ از طریق ایمیل، طی مدت ۲۴ ساعت، پاسخگویی تلفنی از طریق شماره  ۰۹۱۶۲۴۸۳۶۰۳  از ساعت ۱۶ الی  ۱۸ امکان‌پذیر است. (همچنین می‌توانید از طریق تلگرام شماره مذکور نیز اقدام نمایید).
- پاسخگویی حضوری در دفتر شهاب
پاسخگویی حضوری در دفتر شهاب هر روز از شنبه تا چهارشنبه، از ساعت ۸ تا ۱۳:۳۰ امکان‌پذیر است.
"
);

$sign_up_questions  = array(
"q1"=>"۱- هدف از طرح خوشه بندی چیه؟",
"q2"=>"۲- شرایط و نحوه ثبت نام چطوریه؟",
"q3"=>"۳- در صورت احراز چند بند، همه رو انتخاب کنم یا یکی کافیه؟",
"q4"=>"۴- احراز شرایط برتر پژوهشی یعنی چی؟",
"q5"=>"۵- سنوات مجاز تحصیلی برای شرکت در این طرح چگونه هست؟",
"q6"=>"۶- دانشجوی سال آخر هستم، میتونم در طرح ثبت نام کنم؟",
"q7"=>"۷- آیا برای استفاده از تسهیلات بنیاد نخبگان، ثبت نام در طرح خوشه بندی الزامیه؟",
"q8"=>"۸- در چه بازه زمانی میتونم از بسته های تشویقی استفاده کنم؟",
"q9"=>"۹- گواهی رتبه معدل و ۱۰ درصد برتر چیه؟",
"q10"=>"۱۰- اگر از دانشگاه دیگه در دانشگاه فردوسی قبول بشم، آیا گواهی استعداد درخشان برام صادر میشه؟",
"q11"=>"۱۱- فرم گواهی معدل رو از کجا تهیه کنم؟",
"q12"=>"۱۲- تاییدیه پذیرش بدون آزمون رو از کجا بگیرم؟",
"q13"=>"۱۳- آیا ثبت نام در هر دوره از طرح الزامیه؟",
"q14"=>"۱۴- دانشجویان بین الملل و مجازی میتونن در طرح ثبت نام کنن؟",
"q15"=>"۱۵- آیا افتخارات کسب شده در مقطه دبیرستان مورد ارزیابی قرار می گیره؟",
"q16"=>"۱۶- آیا سامانه سینا همان سامانه ثریا هست؟",
"q17"=>"۱۷- آیا با افتخارات قرآنی می توان در طرح ثبت نام کرد؟",
"q18"=>"۱۸- نتایج ثبت نام کی اعلام میشه؟",
"q19"=>"۱۹- چطوری به نتیجه خوشه بندی اعتراض کنم؟",
"q20"=>"۲۰- بر چه اساسی دانشجویان در سه خوشه ۱، ۲ و ۳ قرار میگیرن؟",
"q21"=>"۲۱- نماینده شهاب دانشکده ها رو بهم میگی",
"q22"=>"۲۲- راههای ارتباطی با دفتر شهاب چیه؟",
);

$facility_strings   = array(
"fa1"=>"
حمایت از شرکت دانشجویان در كارگاه‏های آموزشی معتبر داخلی (یکبار در طول دوره یکساله ‏طرح خوشه‌بندی شهاب)

- خوشه ۱: کارشناسی (۴۰۰ هزار تومان)، کارشناسی ارشد (۵۰۰ هزار تومان)، دکتری حرفه ای (۵۰۰ هزار تومان) و دکتری تخصصی (۶۰۰ هزار تومان)
- خوشه ۲: کارشناسی (۳۰۰ هزار تومان)، کارشناسی ارشد (۴۰۰ هزار تومان)، دکتری حرفه ای (۴۰۰ هزار تومان) و دکتری تخصصی (۵۰۰ هزار تومان)
- خوشه ۳: کارشناسی (۲۰۰ هزار تومان)، کارشناسی ارشد (۳۰۰ هزار تومان)، دکتری حرفه ای (۳۰۰ هزار تومان) و دکتری تخصصی (۴۰۰ هزار تومان)


حمایت از شرکت دانشجویان در همایش/کنگره‏های معتبر داخلی با ارائه سخنرانی برای یکبار شرکت دانشجو در طول دوره طرح خوشه‏بندی شهاب

- خوشه ۱: کارشناسی (۴۰۰ هزار تومان)، کارشناسی ارشد (۵۰۰ هزار تومان)، دکتری حرفه ای (۵۰۰ هزار تومان) و دکتری تخصصی (۶۰۰ هزار تومان)
- خوشه ۲: کارشناسی (۳۰۰ هزار تومان)، کارشناسی ارشد (۴۰۰ هزار تومان)، دکتری حرفه ای (۴۰۰ هزار تومان) و دکتری تخصصی (۵۰۰ هزار تومان)
- خوشه ۳: کارشناسی (۲۰۰ هزار تومان)، کارشناسی ارشد (۳۰۰ هزار تومان)، دکتری حرفه ای (۳۰۰ هزار تومان) و دکتری تخصصی (۴۰۰ هزار تومان)


حمایت و ترغیب دانشجویان برای برگزاری برنامه‏های تخصصی پرورش استعدادهای خاص علمی، ورزشی، هنری و فرهنگی تحت سرپرستی اعضای هیات علمی
تعرفه این بند حداکثر تاسقف ۴,۰۰۰,۰۰۰ میلیون تومان می‏باشد که متناسب با نوع برنامه، جامعه هدف و نتایج آن پس از تایید شورای مرکزی شهاب(فرم تایید اولیه شهاب) به دانشجوی محری تعلق خواهد پذیرفت.
نکته: در این بند دانشجویان بنا بر توانمندی‏های خود و با تایید یکی از اعضای هیئت علمی  برنامه‏ای تخصصی را برای پرورش استعدادهای خاص در قالب فرم تایید اولیه شهاب، پیشنهاد می‏دهد.
جامعه هدف در این برنامه می‏تواند دانشجویان خوشه‏های پایین‏تر نسبت به خوشه‏ی دانشجوی مجری بوده و هدف برنامه ارتقا سطح و پرورش استعدادهای خاص در حوزه‏های مختلف باشد

حمایت از ارایه برنامه‏های توانمند سازی توسط دانشجویان طرح خوشه‏بندی شهاب با هماهنگی معاونت آموزشی دانشكده و تایید شورای مركزی شهاب و تعیین ناظر. لازم به ذكر است برنامه مذكور نباید قالب كارگاهی داشته باشد و نوآوری شرط اصلی ارائه برنامه می‏باشد
حمایت از ارایه سخنرانی‏های علمی به سبک TED توسط دانشجویان طرح خوشه‏بندی شهاب (مطابق با هزینه‏های شركت در همایش‏های علمی با سخنرانی) با تایید معاونت آموزشی دانشكده و شورای شهاب
دانشجویان متقاضی استفاده از خوابگاه متاهلین در صورت پذیرش در طرح خوشه‌بندی می‏توانند با شرایط زیر اضافه بر سنوات مجاز از تسهیلات خوابگاه استفاده نمایند

خوشه ۱: تمدید ۳ نیمسال تحصیلی اضافه بر سنوات مجاز
خوشه ۲: تمدید ۲ نیمسال تحصیلی اضافه بر سنوات مجاز
خوشه ۳: تمدید ۱ نیمسال تحصیلی اضافه بر سنوات مجاز


تسهیلات استفاده از فرصت مطالعات
در راستای حمایت از فعالیت‏های دانشجویان در حوزه‏های بین‏المللی و گسترش ارتباطات علمی با سایر مراکز و دانشگاه‏های معتبر خارج از کشور، تسهیلات حمایت از دوره‏های فرصت مطالعاتی دانشجویان طرح خوشه‏بندی شهاب توسط گروه شناسایی و هدایت استعدادهای برتر دانشگاه و طبق مصوبه شورای شهاب اختصاص داده شده است:
مبلغ حمایت از دانشجویان دكتری تخصصی خوشه ۱ تا مبلغ ۲۰,۰۰۰,۰۰۰ ریال، خوشه۲ تا مبلغ ۱۵,۰۰۰,۰۰۰ ریال و خوشه ۳ تا مبلغ ۱۰.۰۰۰.۰۰۰ ریال می‏باشد",
"fa2"=>"
۲- مراحل ثبت درخواست رو بهم میگی؟
۱- درخواست دانشجو: تکمیل مدارک مورد نیاز (موجود در سایت)
۲- تایید مدارک توسط کارشناس شهاب
۳- تایید و امضا مدیر دفتر استعدادهای درخشان
4- تایید و امضا معاونت آموزشی دانشگاه
۵- امضا کاربرگ مالی توسط دانشجو
۶- تحویل حسابداری (دریافت کد پیگیری)",
"fa3"=>"
۳- مدارک استفاده از تسهیلات چی هستن؟
۳-۱- مدارک مورد نیاز برای كارگاه‏های آموزشی معتبر داخلی
تكمیل فرم درخواست بسته تشویقی، نامه تایید استاد راهنما ( با ذکر موارد، ارائه به صورت سخنرانی و در راستای پایان نامه بودن مقاله)، اصل و تصویر گواهی شركت در كارگاه، مدارك مثبته مالی
۳-۲- مدارک مورد نیاز برای شرکت دانشجویان در همایش/کنگره‏های معتبر داخلی با ارائه سخنرانی
تکمیل فرم درخواست بسته تشویقی، نامه تایید استاد راهنما ( با ذکر موارد، ارائه به صورت سخنرانی و در راستای پایان نامه بودن مقاله)، اصل و تصویر گواهی شرکت که نوع ارائه به صورت سخنرانی در آن مشخص شده باشد، مدارک مثبته مالی
۳-۳- مدارک مورد نیاز برای برگزاری برنامه‏های تخصصی پرورش استعدادهای خاص علمی، ورزشی، هنری و فرهنگی تحت سرپرستی اعضای هیات علمی
تکمیل فرم درخواست بسته تشویقی، تکمیل فرم تایید اولیه شهاب که در آن برنامه به صورت کامل معرفی گردیده است، نامه تایید عضو هیات علمی به عنوان سرپرست برنامه
۳-۴- مدارک مورد نیاز برای فرصت های مطالعاتی
۱) ارایه كاربرگ استفاده از فرصت مطالعاتی خارج كشور( فرم ۱۰۱  ) به صورت تكمیل و ممهور شده
۲) ارایه فرم تعیین رتبه دانشگاه مقصد در نظام‎های رتبه‎بندی سه گانه
 ( QS ، TIMES ، Shanghai)
۳) مدارك مربوط به خوشه‎بندی و مستندات مالی",
"fa4"=>"۴- راههای ارتباطی با دفتر شهاب چیه؟
۴-۱- پاسخگویی از طریق ایمیل
دانشجویان محترم توجه داشته باشند که در صورت داشتن هرگونه سوال می توانند از طریق ایمیل و با ذکر عنوان طرح خوشه بندی شهاب  اقدام نمایند. Talent2@um.ac.ir
۴-۲- پاسخگویی تلفنی
در صورت دریافت نکردن پاسخ از طریق ایمیل، طی مدت ۲4 ساعت، پاسخگویی تلفنی از طریق شماره  ۰۹۱۶۲۴۸۳۶۰۳  از ساعت ۱۶ الی  ۱۸ امکان پذیر می باشد. )همچنین می توانید از طریق تلگرام شماره مذکور نیز اقدام نمایید.(
۴-۳- پاسخگویی حضوری  در دفتر شهاب
پاسخگویی حضوری در دفتر شهاب هر روز از شنبه تا چهارشنبه، از ساعت ۸ تا ۱۳:۳۰ امکانپذیر می باشد."
);

$facility_questions = array(
"f1"=>"۱- تسهیلات خوشه ۱، ۲ و ۳ چیه؟",
"f2"=>"۲- مراحل ثبت درخواست رو بهم میگی؟",
"f3"=>"۳- مدارک استفاده از تسهیلات چی هستن؟",
"f4"=>"۴- راههای ارتباطی با دفتر شهاب چیه؟",
);

$facility_strings_first   = array(
"fqf1"=>"
حمایت از شرکت دانشجویان در كارگاه‏های آموزشی معتبر داخلی (یکبار در طول دوره یکساله ‏طرح خوشه‌بندی شهاب)


- خوشه ۱: کارشناسی
(۴۰۰ هزار تومان)، کارشناسی ارشد (۵۰۰ هزار تومان)، دکتری حرفه ای (۵۰۰ هزار تومان) و دکتری تخصصی (۶۰۰ هزار تومان)

- خوشه ۲: کارشناسی
(۳۰۰ هزار تومان)، کارشناسی ارشد (۴۰۰ هزار تومان)، دکتری حرفه ای (۴۰۰ هزار تومان) و دکتری تخصصی (۵۰۰ هزار تومان)

- خوشه ۳: کارشناسی
(۲۰۰ هزار تومان)، کارشناسی ارشد (۳۰۰ هزار تومان)، دکتری حرفه ای (۳۰۰ هزار تومان) و دکتری تخصصی (۴۰۰ هزار تومان)
",

"fqf2"=>"
حمایت از شرکت دانشجویان در همایش/کنگره‏های معتبر داخلی با ارائه سخنرانی برای یکبار شرکت دانشجو در طول دوره طرح خوشه‏بندی شهاب


- خوشه ۱: کارشناسی
(۴۰۰ هزار تومان)، کارشناسی ارشد (۵۰۰ هزار تومان)، دکتری حرفه ای (۵۰۰ هزار تومان) و دکتری تخصصی (۶۰۰ هزار تومان)

- خوشه ۲: کارشناسی
(۳۰۰ هزار تومان)، کارشناسی ارشد (۴۰۰ هزار تومان)، دکتری حرفه ای (۴۰۰ هزار تومان) و دکتری تخصصی (۵۰۰ هزار تومان)

- خوشه ۳: کارشناسی
(۲۰۰ هزار تومان)، کارشناسی ارشد (۳۰۰ هزار تومان)، دکتری حرفه ای (۳۰۰ هزار تومان) و دکتری تخصصی (۴۰۰ هزار تومان)

",

"fqf3"=>"
حمایت و ترغیب دانشجویان برای برگزاری برنامه‏های تخصصی پرورش استعدادهای خاص علمی، ورزشی، هنری و فرهنگی تحت سرپرستی اعضای هیات علمی
تعرفه این بند حداکثر تاسقف ۴,۰۰۰,۰۰۰ میلیون تومان می‏باشد که متناسب با نوع برنامه، جامعه هدف و نتایج آن پس از تایید شورای مرکزی شهاب(فرم تایید اولیه شهاب) به دانشجوی محری تعلق خواهد پذیرفت.
نکته: در این بند دانشجویان بنا بر توانمندی‏های خود و با تایید یکی از اعضای هیئت علمی  برنامه‏ای تخصصی را برای پرورش استعدادهای خاص در قالب فرم تایید اولیه شهاب، پیشنهاد می‏دهد.
جامعه هدف در این برنامه می‏تواند دانشجویان خوشه ‏های پایین‏تر نسبت به خوشه‏ی دانشجوی مجری بوده و هدف برنامه ارتقا سطح و پرورش استعدادهای خاص در حوزه ‏های مختلف باشد.
",

"fqf4"=>"
حمایت از ارایه برنامه‏های توانمند سازی توسط دانشجویان طرح خوشه‏بندی شهاب با هماهنگی معاونت آموزشی دانشكده و تایید شورای مركزی شهاب و تعیین ناظر. لازم به ذكر است برنامه مذكور نباید قالب كارگاهی داشته باشد و نوآوری شرط اصلی ارائه برنامه می‏باشد

تا سقف ۲۰۰۰۰۰۰ تومان
",

"fqf5"=>"
حمایت از ارایه سخنرانی‏های علمی به سبک TED توسط دانشجویان طرح خوشه‏بندی شهاب (مطابق با هزینه‏های شركت در همایش‏های علمی با سخنرانی) با تایید معاونت آموزشی دانشكده و شورای شهاب
",

"fqf6"=>"
دانشجویان متقاضی استفاده از خوابگاه متاهلین در صورت پذیرش در طرح خوشه‌بندی می‏توانند با شرایط زیر اضافه بر سنوات مجاز از تسهیلات خوابگاه استفاده نمایند

خوشه ۱: تمدید ۳ نیمسال تحصیلی اضافه بر سنوات مجاز
خوشه ۲: تمدید ۲ نیمسال تحصیلی اضافه بر سنوات مجاز
خوشه ۳: تمدید ۱ نیمسال تحصیلی اضافه بر سنوات مجاز
",

"fqf7"=>"
تسهیلات استفاده از فرصت مطالعات
در راستای حمایت از فعالیت‏های دانشجویان در حوزه‏های بین‏المللی و گسترش ارتباطات علمی با سایر مراکز و دانشگاه‏های معتبر خارج از کشور، تسهیلات حمایت از دوره‏های فرصت مطالعاتی دانشجویان طرح خوشه‏بندی شهاب توسط گروه شناسایی و هدایت استعدادهای برتر دانشگاه و طبق مصوبه شورای شهاب اختصاص داده شده است:
مبلغ حمایت از دانشجویان دكتری  تخصصی خوشه ۱ تا مبلغ ۲۰,۰۰۰,۰۰۰
ریال، خوشه۲ تا مبلغ ۱۵,۰۰۰,۰۰۰ ریال و خوشه ۳ تا مبلغ ۱۰.۰۰۰.۰۰۰ ریال می‏باشد.
",

 );

$facility_questions_first = array(
    "ff1"=>"شرکت دانشجویان در كارگاه‏های آموزشی",
    "ff2"=>"شرکت دانشجویان در همایش/کنگره‏های معتبر داخلی",
    "ff3"=>"حمایت و ترغیب دانشجویان برای برگزاری برنامه‏های تخصصی",
    "ff4"=>"حمایت از ارایه برنامه‏های توانمند سازی توسط دانشجویان",
    "ff5"=>"حمایت از ارایه سخنرانی‏های علمی به سبک TED",
    "ff6"=>"دانشجویان متقاضی استفاده از خوابگاه متاهلین",
    "ff7"=>"تسهیلات استفاده از فرصت مطالعات"
);

$common_phrases_body = array(
"co1"=>"عدم پذیرش در طرح خوشه بندی
این عبارات نشان دهنده بررسی پرونده   دانشجو توسط داوران و اتمام فرایند داوری می‌باشد و در هر مورد، توضیحات داوران مربوطه نیز درج شده است. در موارد کسب نتیجه عدم پذیرش ، درصورتی که دانشجو استدلال‌های روشن و مدارک معتبر برای ارایه داشته باشد، می‌تواند برای بررسی مجدد پرونده درخواست تجدید نظر نماید. لازم به ذکر است که اولویت بررسی مجدد با درخواست‌های عدم پذیرش بوده و سایر درخواست‌ها در فهرست انتظار باقی می‌ماند. در این حالت پرونده دانشجو به حالت تعلیق در آمده و امکان استفاده از تسهیلات را نخواهد داشت.",
"co2"=>"به معنای آن است که داوران در مرحله گروه پرونده را بررسی کرده‌اند اما در مرحله دانشکده پرونده بررسی نشده و فرایند داوری کامل نشده است. در این حالت دانشجو می‌تواند برای ادامه فرایند داوری و کسب نتیجه نهایی درخواست تجدید نظر خود را ارسال نماید.",
"co3"=>"بررسی نشده
به معنای آن است که پرونده در مرحله گروه بررسی نشده و فرایند داوری کامل نشده است. ( در این حالت دانشجو می‌تواند برای آغاز مجدد فرایند داوری پرونده، درخواست تجدید نظر خود را ارسال نماید."
);

$common_phrases = array(
"co1"=>"عدم پذیرش در طرح خوشه بندی",
"co2"=>"پذیرش در طرح خوشه بندی",
"co3"=>"بررسی نشده"
);


$telegram = new Api('777491541:AAGEzJyjelk4f6cgQKlDHqrAmo6hV1Oiwho');
$telegram->addCommands([
\App\Http\Controllers\Commands\HelpCommand::class,
\App\Http\Controllers\Commands\StartCommand::class,
\App\Http\Controllers\Commands\SignupCommand::class,
]);

$keyboard_facility_questions =
[
[['text' => "بازکشت به منو اصلی 🔙",],],
[['text' => $facility_questions["f1"],],],
[['text' => $facility_questions["f2"],],],
[['text' => $facility_questions["f3"],],],
[['text' => $facility_questions["f4"],],],
];

$keyboard_facility_questions_first = [
    [['text' => "بازکشت به منو اصلی 🔙",],],
    [['text' => $facility_questions_first["ff1"],],],
    [['text' => $facility_questions_first["ff2"],],],
    [['text' => $facility_questions_first["ff3"],],],
    [['text' => $facility_questions_first["ff4"],],],
    [['text' => $facility_questions_first["ff5"],],],
    [['text' => $facility_questions_first["ff6"],],],
    [['text' => $facility_questions_first["ff7"],],],
];

$keyboard_signup_questions =
[
[['text' => "بازکشت به منو اصلی 🔙",],],
[['text' => $sign_up_questions["q1"],],],
[['text' => $sign_up_questions["q2"],],],
[['text' => $sign_up_questions["q3"],],],
[['text' => $sign_up_questions["q4"],],],
[['text' => $sign_up_questions["q5"],],],
[['text' => $sign_up_questions["q6"],],],
[['text' => $sign_up_questions["q7"],],],
[['text' => $sign_up_questions["q9"],],],
[['text' => $sign_up_questions["q10"],],],
[['text' => $sign_up_questions["q11"],],],
[['text' => $sign_up_questions["q12"],],],
[['text' => $sign_up_questions["q13"],],],
[['text' => $sign_up_questions["q14"],],],
[['text' => $sign_up_questions["q15"],],],
[['text' => $sign_up_questions["q16"],],],
[['text' => $sign_up_questions["q17"],],],
[['text' => $sign_up_questions["q18"],],],
[['text' => $sign_up_questions["q19"],],],
[['text' => $sign_up_questions["q20"],],],
[['text' => $sign_up_questions["q21"],],],
[['text' => $sign_up_questions["q22"],],],
];

$keyboard_common_pharases = [
[['text' => "بازکشت به منو اصلی 🔙",],],
[['text' => $common_phrases["co1"],],],
[['text' => $common_phrases["co2"],],],
[['text' => $common_phrases["co3"],],],
];

$keyboard_main =
[
[['text' => "ثبت نام در طرح خوشه بندی 👨🏻‍💻",],['text' => "تسهیلات طرح خوشه بندی🎉",],],
[['text' => "عبارات متداول در طرح خوشه بندی 📚",],['text' => "📲 ارتباط با ما",]],
// [['text' => "انتقادات و پیشنهادات 🤔",]],
];


$reply_markup_main_keyboard = Telegram::replyKeyboardMarkup(['keyboard' => $keyboard_main, 'resize_keyboard' => true, 'one_time_keyboard' => true]);
$reply_sign_up_questions = Telegram::replyKeyboardMarkup(['keyboard' => $keyboard_signup_questions, 'resize_keyboard' => true, 'one_time_keyboard' => true]);
$reply_facility_questions = Telegram::replyKeyboardMarkup(['keyboard' => $keyboard_facility_questions, 'resize_keyboard' => true, 'one_time_keyboard' => true]);
$reply_facility_questions_first = Telegram::replyKeyboardMarkup(['keyboard' => $keyboard_facility_questions_first, 'resize_keyboard' => true, 'one_time_keyboard' => true]);
$reply_common_phrases = Telegram::replyKeyboardMarkup(['keyboard' => $keyboard_common_pharases, 'resize_keyboard' => true, 'one_time_keyboard' => true]);


$telegram->commandsHandler(true);
if ($request->has('message.text')) {
switch ($request->input('message.text')) {
case 'ثبت نام در طرح خوشه بندی 👨🏻‍💻':
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'), 'text' => 'لطفا سوالی که در ذهن خود دارید از دسته بندی های زیر انتخاب کنید', 'reply_markup' => $reply_sign_up_questions]);

case 'تسهیلات طرح خوشه بندی🎉':
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'), 'text' => 'لطفا سوالی که در ذهن خود دارید از دسته بندی های زیر انتخاب کنید', 'reply_markup' => $reply_facility_questions]);

case 'عبارات متداول در طرح خوشه بندی 📚':
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'), 'text' => 'لطفا سوالی که در ذهن خود دارید از دسته بندی های زیر انتخاب کنید', 'reply_markup' => $reply_common_phrases]);

case 'انتقادات و پیشنهادات 🤔':
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'), 'text' => 'هم اکنون امکان ارسال انتقادات و پیشنهادات وجود ندارد', 'reply_markup' => $reply_markup_main_keyboard]);

case '📲 ارتباط با ما':
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'), 'text' => $sign_up_strings["qu22"], 'reply_markup' => $reply_markup_main_keyboard]);




// states that are for sign up
case $sign_up_questions["q1"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu1"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q2"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu2"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q3"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu3"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q4"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu4"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q5"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu5"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q6"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu6"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q7"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu7"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q8"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu8"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q9"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu9"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q10"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu10"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q11"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu11"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q12"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu12"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q13"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu13"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q14"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu14"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q15"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu15"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q16"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu16"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q17"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu17"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q18"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu18"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q19"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu19"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q20"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu20"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q21"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu21"], 'reply_markup' => $reply_sign_up_questions]);
case $sign_up_questions["q22"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $sign_up_strings["qu22"], 'reply_markup' => $reply_sign_up_questions]);


// states that are for facilities

case $facility_questions["f1"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => "گزینه مورد نظر خود را انتخاب کنید", 'reply_markup' => $reply_facility_questions_first]);
case $facility_questions["f2"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $facility_strings["fa2"], 'reply_markup' => $reply_facility_questions]);
case $facility_questions["f3"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $facility_strings["fa3"], 'reply_markup' => $reply_facility_questions]);
case $facility_questions["f4"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $facility_strings["fa4"], 'reply_markup' => $reply_facility_questions]);


// states that are for facilities first

case $facility_questions_first["ff1"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $facility_strings_first["fqf1"], 'reply_markup' => $reply_facility_questions_first]);
case $facility_questions_first["ff2"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $facility_strings_first["fqf2"], 'reply_markup' => $reply_facility_questions_first]);
case $facility_questions_first["ff3"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $facility_strings_first["fqf3"], 'reply_markup' => $reply_facility_questions_first]);
case $facility_questions_first["ff4"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $facility_strings_first["fqf4"], 'reply_markup' => $reply_facility_questions_first]);
case $facility_questions_first["ff5"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $facility_strings_first["fqf5"], 'reply_markup' => $reply_facility_questions_first]);
case $facility_questions_first["ff6"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $facility_strings_first["fqf6"], 'reply_markup' => $reply_facility_questions_first]);
case $facility_questions_first["ff7"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $facility_strings_first["fqf7"], 'reply_markup' => $reply_facility_questions_first]);




// states that are for common_pharases

case $common_phrases["co1"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $common_phrases_body["co1"], 'reply_markup' => $reply_common_phrases]);
case $common_phrases["co2"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $common_phrases_body["co2"], 'reply_markup' => $reply_common_phrases]);
case $common_phrases["co3"]:
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'),'text' => $common_phrases_body["co3"], 'reply_markup' => $reply_common_phrases]);


case "بازکشت به منو اصلی 🔙":
return Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'), 'text' => 'با انتخاب یکی از گزینه ها به روند ادامه دهید', 'reply_markup' => $reply_markup_main_keyboard]);



}


if ($request->has('message.reply_to_message')) {
if ($request->input('message.reply_to_message.text') == 'لطفا نام و نام خانوادگی خود را وارد نمایید.') {

} else if ($request->input('message.reply_to_message.text') == 'لطفا ایمیل خود را وارد کنید') {

} else {

}
}
} else if ($request->has('message.contact')) {

}
}
}
