<?php

namespace App\Http\Controllers\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class SignupCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "signup";

    /**
     * @var string Command Description
     */
    protected $description = "ثبت نام در ربات";

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {
        $request = app('request');


//        $response = Telegram::sendMessage([
//            'chat_id' => $request->input('message.chat.id'),
//            'text' => 'شما در ثبت نام هستید1 =)))'
//        ]);
        $keyboard =
            [
                [
                    [
                        'text' => "👱 نام و نام خانوادگی",
                    ],
                ],
                [
                    [
                        'text' => "📞 ارسال شماره همراه",
                        'request_contact' => true
                    ],
                ],
                [
                    [
                        'text' => "☑️ ثبت ایمیل",
                    ]
                ],
//                [
//                    [
//                        'text' => "♀️ انتخاب جنسیت ♂️",
//                    ]
//                ]
            ]; //user button under keyboard.

        $reply_markup = Telegram::replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => false]);
        Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'), 'text' => '✅برای برقراری هرچه بهتر ارتباط با شما مخاطب گرامی، خواهشمندیم اطلاعات ذیل را تکمیل فرمایید تا در لیست اعضای کانال مصطفی ملکیان قرار گیرید.', 'reply_markup' => $reply_markup]);




//        $this->replyWithMessage(compact('text'));
//        $keyboard = [
//            ['آخرین مقالات سایت', 'آخرین حروف سایت'],
//            ['راهنمای استفاده از ربات'],
//        ];

//        $reply_markup = Telegram::replyKeyboardMarkup([
//            'keyboard' => $keyboard,
//            'resize_keyboard' => true,
//            'one_time_keyboard' => true
//        ]);


//        Telegram::sendMessage([
//            'chat_id' => $request->input('message.chat.id'),
//            'text' => 'کیبوررررررررد',
//            'reply_markup'=>$reply_markup
//        ]);


//        $messageId = $response->getMessageId();

    }
}
