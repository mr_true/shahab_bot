<?php

namespace App\Http\Controllers\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "start";

    /**
     * @var string Command Description
     */
    protected $description = "برای شروع کلیک کنید";

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {
        // This will send a message using `sendMessage` method behind the scenes to
        // the user/chat id who triggered this command.
        // `replyWith<Message|Photo|Audio|Video|Voice|Document|Sticker|Location|ChatAction>()` all the available methods are dynamically
        // handled when you replace `send<Method>` with `replyWith` and use the same parameters - except chat_id does NOT need to be included in the array.

        // This will update the chat status to typing...
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        // This will prepare a list of available commands and send the user.
        // First, Get an a+rray of all registered commands
        // They'll be in 'command-name' => 'Command Handler Class' format.
        $commands = $this->getTelegram()->getCommands();


        $request = app('request');



        $this->replyWithMessage(['text'=>'سلام من شهابم - خوشحالم از اینکه با من هم صحبت شدی']);
//
//        $this->replyWithMessage(
//          ['text'=>'
//            من سعی میکنم تو این موارد بهت کمک کنم :
//
//
//            ❇️  ثبت نام در طرح خوشه بندی شهاب
//            -------------------------------
//            ❇️ تسهیلات مربوط به طرح خوشه بندی
//            -------------------------------
//            ❇️ عبارات متداول در طرح خوشه بندی
//            -------------------------------
//            ❇️ انتقادات و پیشنهادات
//            -------------------------------
//            ...
//        ']);
          $keyboard =
          [
            [['text' => "ثبت نام در طرح خوشه بندی 👨🏻‍💻",],['text' => "تسهیلات طرح خوشه بندی🎉",],],
            [['text' => "عبارات متداول در طرح خوشه بندی 📚",],['text' => "📲 ارتباط با ما",]],
            // [['text' => "انتقادات و پیشنهادات 🤔",]],
          ];

    $reply_markup = Telegram::replyKeyboardMarkup(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => true]);
    Telegram::sendMessage(['chat_id' => $request->input('message.chat.id'), 'text' => 'با انتخاب یکی از گزینه ها به روند ادامه دهید', 'reply_markup' => $reply_markup]);

    }
}
