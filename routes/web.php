<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/777491541:AAGEzJyjelk4f6cgQKlDHqrAmo6hV1Oiwho/webhook', function () {
    $update = Telegram::commandsHandler(true);
	
	// Commands handler method returns an Update object.
	// So you can further process $update object 
	// to however you want.
	
    return 'ok';
});
Route::get('/main_processor','MainController@index');
Route::post('/main_processor','MainController@index');
Route::get('/set_webhook', 'ConfigureWebHookerController@index');
Route::post('/sajjad','ConfigureCommandsController@index');
Route::get('/set_commands', 'ConfigureCommandsController@index');
